**Perspective**

So we start of with this: 

[https://twitter.com/garybernhardt/status/557663175236399104](https://twitter.com/garybernhardt/status/557663175236399104)

"I never realized that operators could make me mad until the first time I scanned the lens docs. <<//=, <<^^=, <<<>=... dozens of them"

Investigating this further, we find and interesting remark:

[https://twitter.com/garybernhardt/status/557753926553206785](https://twitter.com/garybernhardt/status/557753926553206785)

"The fact that it's an operator isn't important. "<<<>=" is not a meaningful concept. I'm not willing to debate this further, sorry."

But this is the actual interesting bit ! (IMHO) 

So why is this? How came "operators" like "<<//=" to be?  If you want to read up on the more or less entangled version ( depending on your perspective) here there 
Original link : 

[Sticks, stones, but names are not useful to me](http://tonymorris.github.io/blog/posts/identifier-names/index.html)

and a whole talk about the principle:

[Parametricity](http://yowconference.com.au/slides/yowlambdajam2014/Morris-ParametricityTypesAreDocumentation.pdf)

Thats for the references now my hummble attempt at explaining this. People who do "programming" gather an enormous context to be able to do so and
 yet most of the time its not sufficent and misunderstandings happen (https://www.cs.utexas.edu/users/EWD/transcriptions/EWD08xx/EWD854.html) 
So lets assume a programmers background or context to be in the OO/imperative for the majority of his time. And so when he comes to other paradigms like FP, he will discover quite
different fundamental concepts (http://steve-yegge.blogspot.de/2006/03/execution-in-kingdom-of-nouns.html)
We see some things are done as verbs some as nouns.

Here an example snippet of something OO'like Code: 

Customer aCustomer=new Customer();

aCustomer.buy();

So now lets ask the question : Why do we want to name the function?

1. We have to create a label, to invoke our functions on specific data
2. To provide context, what the function _does_ in a specific context

And in my opinion here comes the catch why: ""<<<>=" is not a meaningful concept"
- Its abstract, and (I think) thats a good thing for the functional programmer. He wants to keep his code as general and abstract as possible and only creates specific functions
as really needed (Concept of re-use in FP).  
- Since the FP programmer wants to use lots, and LOTS of functions/combinators their 'Label' can not be too long otherwise he had to much 'noise'.

Now these two points might seem marginal, but let me explain the sheer dimension of scale we are talking about. So if for the usual OO programmer the relation from
function/method name to complexity of type transformation is low, its the exact opposite for the FP programmer! The function identifier becomes less important and the types are
 gaining much more significance. And thats the point you have to understand the types!, to make "<<<>=" a meaningful concept, the type relations, constraints and transformations provide
 more context than the name. So one write a function and then thinks about a _name_ which fits the data?domain? context (OO Approach), while the other says: why bother finding imprecise names for
 what the function does while its _types_ perfectly fine describe what it does (also checked by the compiler! - FP Approach).
 
 As you can see the conceptions of how to gain context from an identifier name,they are quite different in FP and OO. I also think this is the reason why many programmer new to funcitonal
 programming say: uh but readability, or familiarity. Once you understand that there is a spectrum and that if you try to do things on each sides of the spectrum, then you can decide for
yourself which has these advantages or disadvantages. But don't let unfamiliarity scare you from learning and questioning new things!
 
 
 * by _FP_ I mean static typed FP ala Haskell, Ocaml or Scala
 * by _OO_ I mean mostly class based languages like Java,Ruby or C++